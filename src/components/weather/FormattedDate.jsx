import React, { Component } from 'react'

class FormattedDate extends Component {
	constructor(props) {
		super(props);
		this.state = {
			utcDate: props.utcDate,
			formatData: { hour: 'numeric', minute: 'numeric', hour12: true }
		}
	}

	convertDateToString() {
		let date = new Date(this.state.utcDate * 1000);
		return date.toLocaleString(undefined, this.state.formatData);
	}

	render() {
		return <>{this.convertDateToString()}</>
	}
}

export default FormattedDate; 