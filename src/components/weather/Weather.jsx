import React, { Component } from 'react'
import FormattedDate from './FormattedDate'
import FormattedWind from './FormattedWind'
import { buildUrl, capitalizeAllWords } from '../../utils'

class Weather extends Component {
	url = "http://api.openweathermap.org/data/2.5/weather";
	imageUrl = "http://openweathermap.org/img/wn/";

	constructor(props) {
		super(props);
		this.state = {
			urlParams: {
				id: 5957776,
				APPID: 'a1c9480632295babec4aec5c8939bf7d',
				units:'metric',
			},
			error: null,
			isLoaded: false,
			weather: null,
			scale: 5,
			debug: false
		};
	}

	componentDidMount() {
		fetch(
			buildUrl(this.url, this.state.urlParams ), {
				headers: {
					accept: 'application/json'
				}
			}
		)
		.then(res => res.json())
		.then(
			(result) => {
				this.setState({
					isLoaded: true,
					weather: result
				});
			},
			(error) => {
				this.setState({
					isLoaded: true,
					error
				})
			}
		)
	}
	render() {
		const { error, isLoaded, weather } = this.state;
		const widthCss = {
			width: this.state.scale*3 + 'rem',
			height:this.state.scale + 'rem',
			border: '1px solid black',
			position: 'relative'
		}
		if (error) {
			return <div>Error: {error.message}</div>
		} else if (!isLoaded) {
			return <div style={widthCss}>Loading...</div>
		} else {
			const jsonCss = {
				fontSize: '15px',
				fontFamily: `"Lucida Console", Monaco, monospace`,
				color: 'white',
				backgroundColor: 'black'
			};

			return (
				<div id="weather">
					<div style={widthCss}>
						<div className="icon">
							<img 
								src={this.imageUrl + weather.weather[0].icon + "@2x.png"} 
								style={{height: this.state.scale + 'rem', width: this.state.scale + 'rem'}} 
								alt="I Messed Up"/>
						</div>
						<span style={{position: 'absolute', top: 0, fontSize: (this.state.scale*3/5) + 'rem'}}>
							{weather.main.temp}&deg;
						</span>
						<span style={{position: 'absolute', bottom: 0, fontSize: (this.state.scale*1/4) + 'rem'}}>
							{capitalizeAllWords(weather.weather[0].description)}
						</span>
					</div>
					{ this.state.debug &&
						<>
						<br />
						<br />
						<div>
							Sunrise: <FormattedDate utcDate={weather.sys.sunrise} />; Sunset: <FormattedDate utcDate={weather.sys.sunset} />
						</div>
						<div style={{fontSize:'1.5em'}}>
							<FormattedWind wind={weather.wind} />
						</div>
						<div style={jsonCss} >
							{JSON.stringify(weather)}
						</div>
						</>
					}
				</div>
			)
		}
  }
}

export default Weather;