import React, { Component } from 'react'

class FormattedWind extends Component {
	constructor(props) {
		super(props);
		this.state = {
			wind: props.wind,
		}
	}

	render() {
		const { wind } = this.state;
		const windCompass = {
			transform: 'rotate(' + wind.deg + 'deg)',
			display: 'inline-block',
			fontWeight: 'bold'
		};
		return <>Wind: {wind.speed}m/s <span style={windCompass}>&uarr;</span></>
	}
}

export default FormattedWind; 