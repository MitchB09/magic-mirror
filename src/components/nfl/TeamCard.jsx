import React, { Component } from 'react'

class TeamCard extends Component {

	constructor(props) {
		super(props);
		this.state = {
      team: props.team,
      scale: props.scale
    }
  }
  
  render() {
    const widthCss = {
			width: this.state.scale + 'rem',
			height:this.state.scale*2/3 + 'rem',
			border: '1px solid black',
			position: 'relative'
    }
    return (
      <div style={widthCss}>
        {this.props.team.team} {this.props.team.score}
      </div>
    )
  }
}

export default TeamCard;