import React, { Component } from 'react'
import TeamCard from './TeamCard'

class NFLSchedule extends Component {

	constructor(props) {
		super(props);
		this.state = {
			isLoaded: false,
			data: null,
			error: null,
			scale: 10,
			gameIndex: 0
		}
	}

	componentDidMount() {
		fetch(
			'http://www.nfl.com/schedules', {
			headers: {'Accept-Language':'en-GB,en-CA;q=0.8,en-US;q=0.5,en;q=0.3'}})
		.then(res => res.text())
		.then(
			(result) => {
				var domparser = new DOMParser();
				var scheduleHtml = domparser.parseFromString(result, 'text/html');
				this.setState({
					isLoaded: true,
					data: {
						week: scheduleHtml.getElementsByClassName("schedules-header-title")[0].innerText.trim(),
						games: this.parseHtml(scheduleHtml.getElementsByClassName("schedules-table")[0].getElementsByTagName('li'))
					}
				});
			},
			(error) => {
				this.setState({
					isLoaded: true,
					error
				})
			}
		)
	}

	parseHtml(html) {
		let result = [];
		const getValueFromElementByClass = (row, className, defaultValue, index=0) => {
			let element = row.getElementsByClassName(className);
			if (element.length) {
				return element[index].innerText.trim();
			} else {
				return defaultValue;
			}
		}
		var date;
		for (let i = 0; i < html.length; i++) {
			let row = html[i];
			if (row.className === 'schedules-list-date') {
				date = row.innerText.trim();
			} else {
				let time = getValueFromElementByClass(row, 'time', null);
				let period = getValueFromElementByClass(row, 'pm', '');
				let homeTeam = getValueFromElementByClass(row, 'team-name', '');
				let homeScore = getValueFromElementByClass(row, 'team-score', null);
				let awayTeam = getValueFromElementByClass(row, 'team-name', '', 1);
				let awayScore = getValueFromElementByClass(row, 'team-score', null, 1);
				result.push({'date': date, 'time': time + period, 'away': {'team': awayTeam, 'score': awayScore}, 'home': {'team': homeTeam, 'score': homeScore}});
			}
		}
		return(result);
	}

	updateIndex = (change) => {
		this.setState({
			gameIndex: this.state.gameIndex+change 
		});
	}

	render() {
		const { error, isLoaded, data } = this.state;
		const widthCss = {
			width: this.state.scale*3 + 'rem',
			height:this.state.scale + 'rem',
			border: '1px solid black',
			position: 'relative'
		}
		if (error) {
			return <div>Error: {error.message}</div>
		} else if (!isLoaded) {
			return <div style={widthCss}>Loading...</div>
		} else {
			let game = data.games[this.state.gameIndex];
			return (
				<div style={widthCss}>
					<div style={{width: 'inherit', justifyContent: 'space-between', display: 'flex'}}>
						<div width={this.state.scale*1/6 + 'rem'}>
							<button disabled={this.state.gameIndex === 0} onClick={() => {this.updateIndex(-1)}}>{String.fromCharCode(60)}</button>
						</div>
						<div style={{order:1, width: 'inherit', justifyContent: 'inherit', display: 'inherit'}}>
							<div style={{display: 'flex', flexDirection: 'column'}}>
								<span width='100%'>{game.date + ' ' + game.time}</span>
								<div style={{display: 'flex', flexDirection: 'row'}}>
									<TeamCard team={game.away} scale={this.state.scale}/>
									<div>VS</div>
									<TeamCard team={game.home} scale={this.state.scale}/>
								</div>
							</div>
						</div>
						<div disabled={this.state.gameIndex === data.games.length-1} width={this.state.scale*1/6 + 'rem'} style={{order:3}}>
							<button disabled={this.state.gameIndex+1 === data.games.length} onClick={() => {this.updateIndex(+1)}}>{String.fromCharCode(62)}</button>
						</div>
					</div>
				</div>
			)
		}
	}
}

export default NFLSchedule;