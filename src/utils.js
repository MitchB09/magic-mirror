export const buildUrl = (url, params) => {
	var esc = encodeURIComponent;
	var query = Object.keys(params)
			.map(k => `${esc(k)}=${esc(params[k])}`)
			.join('&');
	return url + '?' + query;
}

export const capitalizeAllWords = (phrase) => {
	return phrase.split(" ").map( word => 
		capitalize(word)	
	).join(" ");
}

export const capitalize = (str) => {
	return str[0].toUpperCase() + str.slice(1);
}
