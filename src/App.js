import React from 'react';
import './App.css';
import Weather from './components/weather/Weather'
import NFLSchedule from './components/nfl/NFLSchedule'


function App() {
  return (
    <div className="App">
      <Weather />
      <NFLSchedule />
    </div>
  );
}

export default App;
